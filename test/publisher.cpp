#include <signal/publisher.hpp>

namespace jaf::signal::test::publisher
{
    struct publisher
        : ::jaf::testing::test_suite
    {
    };

    struct bar
    {
    };

    struct foo;

    struct foo_listener
    {
        MOCK_METHOD(void, f, (foo&));
        MOCK_METHOD(void, g, (foo&, int));
        MOCK_METHOD(void, h, (foo&, const bar&));
    };

    struct foo
        : ::jaf::signal::publisher<foo, foo_listener>
    {
    };

    TEST_F(publisher, basic_call)
    {
        auto e = foo{};
        auto m = std::make_shared<foo_listener>();
        auto& mr = *(m.get());

        EXPECT_CALL(mr, f).Times(1);
        EXPECT_CALL(mr, g).Times(0);
        EXPECT_CALL(mr, h).Times(0);

        e.subscribe(m);

        e.publish(&foo_listener::f);

        e.unsubscribe(m);
    }

    TEST_F(publisher, parameter_passing)
    {
        using ::testing::_;

        auto e = foo{};
        auto m = std::make_shared<foo_listener>();
        auto& mr = *(m.get());

        EXPECT_CALL(mr, f).Times(0);
        EXPECT_CALL(mr, g(_, 5)).Times(1);
        EXPECT_CALL(mr, h).Times(0);

        e.subscribe(m);

        e.publish(&foo_listener::g, 5);

        e.unsubscribe(m);
    }

    TEST_F(publisher, avoid_copy)
    {
        using ::testing::_;
        using ::testing::Invoke;

        auto e = foo{};
        auto m = std::make_shared<foo_listener>();
        auto b = bar{};
        auto& mr = *(m.get());


        EXPECT_CALL(mr, f).Times(0);
        EXPECT_CALL(mr, g).Times(0);
        EXPECT_CALL(mr, h).Times(1).WillOnce(Invoke([&](auto&, const auto& arg)
        {
            EXPECT_EQ(std::addressof(arg), std::addressof(b));
        }));

        e.subscribe(m);

        e.publish(&foo_listener::h, b);

        e.unsubscribe(m);
    }

    TEST_F(publisher, unsubscribe)
    {
        auto e = foo{};
        auto m = std::make_shared<foo_listener>();
        auto& mr = *(m.get());

        EXPECT_CALL(mr, f).Times(0);
        EXPECT_CALL(mr, g).Times(0);
        EXPECT_CALL(mr, h).Times(0);

        e.subscribe(m);
        e.unsubscribe(m);

        e.publish(&foo_listener::f);
        e.publish(&foo_listener::g, 5);
        e.publish(&foo_listener::h, bar{});
    }

    TEST_F(publisher, repeat)
    {
        auto e = foo{};

        auto m = std::make_shared<foo_listener>();
        auto& mr = *(m.get());

        EXPECT_CALL(mr, f).Times(2);
        EXPECT_CALL(mr, g).Times(0);
        EXPECT_CALL(mr, h).Times(1);

        e.subscribe(m);

        e.publish(&foo_listener::f);
        e.publish(&foo_listener::f);
        e.publish(&foo_listener::h, bar{});
        
        e.unsubscribe(m);
    }

    TEST_F(publisher, multiple)
    {
        auto e = foo{};

        auto m1 = std::make_shared<foo_listener>();
        auto& m1r = *(m1.get());

        auto m2 = std::make_shared<foo_listener>();
        auto& m2r = *(m2.get());

        auto m3 = std::make_shared<foo_listener>();
        auto& m3r = *(m3.get());

        EXPECT_CALL(m1r, f).Times(1);
        EXPECT_CALL(m1r, g).Times(0);
        EXPECT_CALL(m1r, h).Times(0);

        EXPECT_CALL(m2r, f).Times(1);
        EXPECT_CALL(m2r, g).Times(0);
        EXPECT_CALL(m2r, h).Times(0);

        EXPECT_CALL(m3r, f).Times(1);
        EXPECT_CALL(m3r, g).Times(0);
        EXPECT_CALL(m3r, h).Times(0);

        e.subscribe(m1);
        e.subscribe(m2);
        e.subscribe(m3);

        e.publish(&foo_listener::f);
        
        e.unsubscribe(m1);
        e.unsubscribe(m2);
        e.unsubscribe(m3);
    }

    TEST_F(publisher, multiple_unsubscribe)
    {
        auto e = foo{};

        auto m1 = std::make_shared<foo_listener>();
        auto& m1r = *(m1.get());

        auto m2 = std::make_shared<foo_listener>();
        auto& m2r = *(m2.get());

        auto m3 = std::make_shared<foo_listener>();
        auto& m3r = *(m3.get());

        EXPECT_CALL(m1r, f).Times(1);
        EXPECT_CALL(m1r, g).Times(1);
        EXPECT_CALL(m1r, h).Times(0);

        EXPECT_CALL(m2r, f).Times(1);
        EXPECT_CALL(m2r, g).Times(0);
        EXPECT_CALL(m2r, h).Times(0);

        EXPECT_CALL(m3r, f).Times(1);
        EXPECT_CALL(m3r, g).Times(1);
        EXPECT_CALL(m3r, h).Times(0);

        e.subscribe(m1);
        e.subscribe(m2);
        e.subscribe(m3);

        e.publish(&foo_listener::f);

        e.unsubscribe(m2);

        e.publish(&foo_listener::g, 3);
        
        e.unsubscribe(m1);
        e.unsubscribe(m3);
    }
}