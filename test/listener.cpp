#include <signal/listener.hpp>

namespace jaf::signal::test::listener
{
    struct listener
        : ::jaf::testing::test_suite
    {
    };

    struct bar
    {
    };

    struct foo
    {
        struct listener_asd
        {
        };

        MOCK_METHOD(void, f, ());
        MOCK_METHOD(void, g, (const bar&));
        
        MOCK_METHOD(void, subscribe, (std::weak_ptr<listener_asd>));
        MOCK_METHOD(void, unsubscribe, (std::weak_ptr<listener_asd>));
    };
    
    TEST_F(listener, auto_unsubscribe)
    {
        auto f = std::make_shared<foo>();
        auto& fr = *(f.get());

        EXPECT_CALL(fr, subscribe).Times(1);
        EXPECT_CALL(fr, unsubscribe).Times(1);

        auto e = ::jaf::signal::listener<foo>{};
        e.add(f);
    }

    TEST_F(listener, manual_unsubscribe)
    {
        auto f = std::make_shared<foo>();
        auto& fr = *(f.get());

        EXPECT_CALL(fr, subscribe).Times(1);
        EXPECT_CALL(fr, unsubscribe).Times(1);

        auto e = ::jaf::signal::listener<foo>{};
        e.add(f);
        e.remove(f);
    }

    TEST_F(listener, multiple_subscribe)
    {
        auto f = std::make_shared<foo>();
        auto& fr = *(f.get());

        EXPECT_CALL(fr, subscribe).Times(1);
        EXPECT_CALL(fr, unsubscribe).Times(1);

        auto e = ::jaf::signal::listener<foo>{};
        e.add(f);
        e.add(f);
        e.add(f);
    }

    TEST_F(listener, resubscribe)
    {
        auto f = std::make_shared<foo>();
        auto& fr = *(f.get());

        EXPECT_CALL(fr, subscribe).Times(2);
        EXPECT_CALL(fr, unsubscribe).Times(2);

        auto e = ::jaf::signal::listener<foo>{};
        e.add(f);
        e.remove(f);

        e.add(f);
    }

    TEST_F(listener, multiple_subscriber)
    {
        auto f1 = std::make_shared<foo>();
        auto& f1r = *(f1.get());

        auto f2 = std::make_shared<foo>();
        auto& f2r = *(f2.get());

        EXPECT_CALL(f1r, subscribe).Times(1);
        EXPECT_CALL(f1r, unsubscribe).Times(1);

        EXPECT_CALL(f2r, subscribe).Times(1);
        EXPECT_CALL(f2r, unsubscribe).Times(1);

        auto e = ::jaf::signal::listener<foo>{};
        e.add(f1);
        e.add(f2);
    }

    TEST_F(listener, multiple_publishers)
    {
        auto f = std::make_shared<foo>();
        auto& fr = *(f.get());

        EXPECT_CALL(fr, subscribe).Times(2);
        EXPECT_CALL(fr, unsubscribe).Times(2);

        auto e1 = ::jaf::signal::listener<foo>{};
        auto e2 = ::jaf::signal::listener<foo>{};

        e1.add(f);
        e2.add(f);
    }

    TEST_F(listener, multiple_publishers_subscribers)
    {
        auto f1 = std::make_shared<foo>();
        auto& f1r = *(f1.get());

        EXPECT_CALL(f1r, subscribe).Times(2);
        EXPECT_CALL(f1r, unsubscribe).Times(2);

        auto f2 = std::make_shared<foo>();
        auto& f2r = *(f2.get());

        EXPECT_CALL(f2r, subscribe).Times(2);
        EXPECT_CALL(f2r, unsubscribe).Times(2);

        auto e1 = ::jaf::signal::listener<foo>{};
        auto e2 = ::jaf::signal::listener<foo>{};

        e1.add(f1);
        e2.add(f1);
        e1.add(f2);
        e2.add(f2);
    }

    TEST_F(listener, address_manual)
    {
        using ::testing::Invoke;

        auto f1 = std::make_shared<foo>();
        auto& f1r = *(f1.get());

        auto f2 = std::make_shared<foo>();
        auto& f2r = *(f2.get());

        auto e = std::make_shared<::jaf::signal::listener<foo>>();

        const auto checker = [&](auto p)
        {
            const auto ptr = p.lock();

            EXPECT_EQ(ptr, e);
        };

        EXPECT_CALL(f1r, subscribe).Times(1).WillOnce(Invoke(checker));
        EXPECT_CALL(f1r, unsubscribe).Times(1).WillOnce(Invoke(checker));

        EXPECT_CALL(f2r, subscribe).Times(1).WillOnce(Invoke(checker));
        EXPECT_CALL(f2r, unsubscribe).Times(1).WillOnce(Invoke(checker));

        e->add(f1);
        e->add(f2);

        e->remove(f1);
        e->remove(f2);
    }

    TEST_F(listener, address_automatic)
    {
        using ::testing::Invoke;

        auto f1 = std::make_shared<foo>();
        auto& f1r = *(f1.get());

        auto f2 = std::make_shared<foo>();
        auto& f2r = *(f2.get());

        auto e = std::make_shared<::jaf::signal::listener<foo>>();

        const auto checker = [&](auto p)
        {
            const auto ptr = p.lock();

            EXPECT_EQ(ptr, e);
        };

        const auto null_checker = [&](auto p)
        {
            const auto ptr = p.lock();

            EXPECT_EQ(ptr, nullptr);
        };

        EXPECT_CALL(f1r, subscribe).Times(1).WillOnce(Invoke(checker));
        EXPECT_CALL(f1r, unsubscribe).Times(1).WillOnce(Invoke(null_checker));

        EXPECT_CALL(f2r, subscribe).Times(1).WillOnce(Invoke(checker));
        EXPECT_CALL(f2r, unsubscribe).Times(1).WillOnce(Invoke(null_checker));

        e->add(f1);
        e->add(f2);
    }

    TEST_F(listener, call)
    {
        using ::testing::Invoke;

        auto f1 = std::make_shared<foo>();
        auto& f1r = *(f1.get());

        auto f2 = std::make_shared<foo>();
        auto& f2r = *(f2.get());

        auto e = ::jaf::signal::listener<foo>();

        EXPECT_CALL(f1r, subscribe).Times(1);
        EXPECT_CALL(f1r, unsubscribe).Times(1);

        EXPECT_CALL(f2r, subscribe).Times(1);
        EXPECT_CALL(f2r, unsubscribe).Times(1);

        e.add(f1);
        e.add(f2);


        EXPECT_CALL(f1r, f).Times(1);
        EXPECT_CALL(f2r, f).Times(2);

        e.call(&foo::f);

        e.remove(f1);

        e.call(&foo::f);
    }
    
    TEST_F(listener, call_address)
    {
        using ::testing::Invoke;

        auto f1 = std::make_shared<foo>();
        auto& f1r = *(f1.get());

        auto f2 = std::make_shared<foo>();
        auto& f2r = *(f2.get());

        auto e = ::jaf::signal::listener<foo>();
        const auto b = bar{};

        EXPECT_CALL(f1r, subscribe).Times(1);
        EXPECT_CALL(f1r, unsubscribe).Times(1);

        EXPECT_CALL(f2r, subscribe).Times(1);
        EXPECT_CALL(f2r, unsubscribe).Times(1);
        
        e.add(f1);
        e.add(f2);

        const auto checker = [&](const auto& p)
        {
            EXPECT_EQ(std::addressof(p), std::addressof(b));
        };
        
        EXPECT_CALL(f1r, g).WillOnce(Invoke(checker));
        EXPECT_CALL(f2r, g).WillOnce(Invoke(checker));

        e.call(&foo::g, b);
    }
}