#pragma once

#include <signal/logger.hpp>

#include <memory>
#include <set>
#include <functional>

namespace jaf::signal
{
    template<class RealType, class Listener>
    struct publisher
    {
        using listener_asd = Listener;

        virtual ~publisher() = default;

        void subscribe(std::weak_ptr<Listener> l)
        {
            TRACE(logger, this);

            l_.insert(l);
        }

        void unsubscribe(std::weak_ptr<Listener> l)
        {
            TRACE(logger, this);

            l_.erase(l);
        }

        template<class... Args>
        using member_function = void(Listener::*)(Args...);

        template<class... FunArgs, class... Args>
        void publish(member_function<FunArgs...>&& f, Args&&... args)
        {
            TRACE(logger, this);

            for(auto l : l_)
            {
                auto lp = l.lock();
                if (nullptr != lp)
                {
                    std::invoke(f, lp, static_cast<RealType&>(*this), std::forward<Args>(args)...);
                }
            }
        }

    private:
        struct weak_ptr_comp
        {
            bool operator()(std::weak_ptr<Listener> lhs, std::weak_ptr<Listener> rhs) const
            { 
                return lhs.owner_before(rhs);
            }
        };

        std::set<std::weak_ptr<Listener>, weak_ptr_comp> l_;
    };
}