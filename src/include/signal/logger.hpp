#pragma once

#include <logging/component.hpp>
#include <logging/logging.hpp>

namespace jaf::signal
{
    struct tag
    {
        static constexpr auto component_name = "signal";
    };

    using logger = logging::component<tag>;
}
