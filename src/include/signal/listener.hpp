#pragma once

#include <signal/logger.hpp>

#include <memory>
#include <set>
#include <functional>

namespace jaf::signal
{
    template<class Publisher>
    struct listener 
        : Publisher::listener_asd
        , std::enable_shared_from_this<listener<Publisher>>
    {
        using base = std::enable_shared_from_this<listener<Publisher>>;

        virtual void add(std::shared_ptr<Publisher> p)
        {
            TRACE(logger, this, p.get());

            const auto& [it, success] = p_.insert(p);
            if (success)
            {
                (*it)->subscribe(base::weak_from_this());
            }
        }

        virtual void remove(std::shared_ptr<Publisher> p)
        {
            TRACE(logger, this, p.get());

            p->unsubscribe(base::weak_from_this());
            p_.erase(std::move(p));
        }

        virtual ~listener()
        {
            for(auto it = p_.begin(); it != p_.end(); it = p_.erase(it))
            {
                (*it)->unsubscribe(base::weak_from_this());
            }
        }

        template<class... Args>
        using member_function = void(Publisher::*)(Args...);

        template<class... FunArgs, class... Args>
        void call(member_function<FunArgs...>&& f, Args&&... args)
        {
            TRACE(logger);
            
            for(auto& p : p_)
            {
                std::invoke(f, p, std::forward<Args>(args)...);
            }
        }

    private:
        std::set<std::shared_ptr<Publisher>> p_;
    };
}
